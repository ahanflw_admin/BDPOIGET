﻿using GMap.NET;
using GMap.NET.WindowsForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DBPOIGET.Classes
{
    public class GMapMarkerCircle:GMapMarker, ISerializable
    {
        public int Radius;

        [NonSerialized]
        public Pen Stroke = new Pen(Color.FromArgb(155, Color.MidnightBlue));

        [NonSerialized]
        public Brush Fill = new SolidBrush(Color.FromArgb(155, Color.AliceBlue));

        public bool IsFilled = true;

        public GMapMarkerCircle(PointLatLng p)
           : base(p)
        {
            Radius = 888; // 888m
            IsHitTestVisible = false;
        }

        public override void OnRender(Graphics g)
        {
            int R = (int)((Radius) / Overlay.Control.MapProvider.Projection.GetGroundResolution((int)Overlay.Control.Zoom, Position.Lat)) * 2;

            if (IsFilled)
            {
                g.FillEllipse(Fill, new System.Drawing.Rectangle(LocalPosition.X - R / 2, LocalPosition.Y - R / 2, R, R));
            }
            g.DrawEllipse(Stroke, new System.Drawing.Rectangle(LocalPosition.X - R / 2, LocalPosition.Y - R / 2, R, R));
        }

        public override void Dispose()
        {
            if (Stroke != null)
            {
                Stroke.Dispose();
                Stroke = null;
            }

            if (Fill != null)
            {
                Fill.Dispose();
                Fill = null;
            }

            base.Dispose();
        }

        #region ISerializable Members

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            // TODO: Radius, IsFilled
        }

        protected GMapMarkerCircle(SerializationInfo info, StreamingContext context)
           : base(info, context)
        {
            // TODO: Radius, IsFilled
        }

        #endregion
    }
}
