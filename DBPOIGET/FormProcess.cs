﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBPOIGET
{
    delegate void NoneParaDelegate();

    public partial class FormProcess : Form
    {
        /// <summary>
        /// 停止的事件，实际上也就是停止下载按钮点击时的触发事件
        /// </summary>
        public event EventHandler Stopped;

        public FormProcess()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 设置下载数量
        /// </summary>
        /// <param name="count"></param>
        public void setCount(int count)
        {

            if (this.InvokeRequired)
            {
                this.Invoke(new NoneParaDelegate(() =>
                {
                    this.lb_count.Text = count.ToString();
                    this.progressBar1.Value = count % 100;
                }));
            }
            else
            {
                this.lb_count.Text = count.ToString();
                this.progressBar1.Value = count % 100;
            }
        }

        public void Close2()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new NoneParaDelegate(() =>
                {
                    this.Close();
                }));
            }
            else
            {
                this.Close();
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            if (this.Stopped != null)
            {
                this.Stopped(this, new EventArgs());
            }
            this.Close();
        }
    }
}
